LIBERO_IMAGE := libero
MICROCHIP_TOOLS_NETWORK := microchip_tools_network
MICROCHIP_TOOLS_POD := microchip_tools_pod
LIBERO_CONTAINER ::= localhost/$(LIBERO_IMAGE):latest

C_RED=		\033[0;31m
C_YELLOW=	\033[0;33m
C_GREEN=	\033[0;32m
C_END=		\033[0m

build:
	podman build . -t libero

daemon: build
	podman run -dt --replace --name libero \
	--net host \
	-v "/etc/localtime:/etc/localtime" \
	-v "${PWD}/licenses:/opt/microchip/licenses" \
	-v "${XAUTHORITY}:/home/libby/.Xauthority:ro" \
	-v "/tmp/.X11-unix:/tmp/.X11-unix:ro" \
	-e "DISPLAY" --security-opt label=type:container_runtime_t \
	${LIBERO_CONTAINER}

# launch a bash shell in the libero container
debug:
	podman exec -it -e "DISPLAY" libero bash

run:
	podman exec -it libero bash -cli libero
