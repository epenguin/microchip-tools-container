FROM ubuntu:22.04

RUN useradd -r -s /sbin/nologin -c "Microchip tools" -m libby
USER root

ENV LIBERO_VERSION="v2024.2"
ENV SOFTCONSOLE_VERSION="v2022.2-RISC-V-747"
ENV BASE_DIR="/opt/microchip"
ARG DEBIAN_FRONTEND=noninteractive
ENV XDG_RUNTIME_DIR="/tmp/.xdg/${UID}"

# install dirs
ENV BASE_DIR="/opt/microchip"
ENV INSTALLERS_DIR="${BASE_DIR}/installers"
ENV SCRIPTS_DIR="${BASE_DIR}/scripts"
ENV LIBERO_DIR="${BASE_DIR}/Libero_SoC_${LIBERO_VERSION}"
ENV SC_DIR="${BASE_DIR}/SoftConsole-${SOFTCONSOLE_VERSION}"
ENV LICENSE_DIR="${BASE_DIR}/licenses"
ENV LOG_DIR="${BASE_DIR}/logs"
ENV FPGENPROG="${LIBERO_DIR}/Libero/bin64/fpgenprog"

# prepare the install dirs
RUN echo "Installer daemons to ${BIN_DIR}..."
RUN mkdir -p ${LICENSE_DIR}
RUN mkdir -p ${INSTALLERS_DIR}
RUN mkdir -p ${SCRIPTS_DIR}
RUN mkdir -p ${LOG_DIR}
RUN chown -R libby:libby ${BASE_DIR}
RUN mkdir -p $XDG_RUNTIME_DIR

ARG DEBIAN_FRONTEND=noninteractive
RUN dpkg --add-architecture i386 && \
    apt-get update -y && \
    apt-get upgrade -y && \
    apt-get install -y libxext6 libx11-6 libxrender1 libxtst6 libxi6 lsb default-jre build-essential

ADD installers/* ${BASE_DIR}/installers/
ADD scripts/* ${BASE_DIR}/scripts/
# run installer in silent mode using config file
RUN echo "Installing Libero... This could take a bit"
RUN ${BASE_DIR}/installers/Libero_SoC_${LIBERO_VERSION}*.bin -i silent -f ${BASE_DIR}/installers/installer.cfg


# RUN echo "Installing SoftConsole... This could take a bit"
# RUN ${BASE_DIR}/installers/Microchip-SoftConsole-${SOFTCONSOLE_VERSION}-linux-x64-installer.run \
#     --mode unattended --unattendedmodeui minimal --installrenodesources "Yes, I accept Renode's MIT license." \
#     --prefix ${SC_INSTALL_DIR}

# 32 bit deps
RUN apt-get install -y libc6:i386 libdrm2:i386 libexpat1:i386 \
    libfontconfig1:i386 libfreetype6:i386 libglapi-mesa:i386 \
    libglib2.0-0:i386 libgl1:i386 libice6:i386 \
    libsm6:i386 libuuid1:i386 libx11-6:i386 \
    libx11-xcb1:i386 libxau6:i386 libxcb-dri2-0:i386 \
    libxcb-glx0:i386 libxcb1:i386 libxdamage1:i386 \
    libxext6:i386 libxfixes3:i386 libxrender1:i386 libxxf86vm1:i386

# gui deps i think?
RUN apt-get install -y libasound2 libasyncns0 libdbus-1-3 \
    libdrm2 libegl1 libexpat1 \
    libflac8 libfontconfig1 libfreetype6 \
    libgbm1 libglapi-mesa libglib2.0-0 \
    libgl1 libice6 libnspr4 \
    libnss3 libogg0 libpulse0 \
    libsm6 libsndfile1 libsqlite3-0 \
    libvorbisenc2 libvorbis0a libwrap0 \
    libx11-xcb1 libxcb-dri2-0 libxcb-glx0 \
    libxcb-render0 libxcb-shape0 libxcb-xfixes0 \
    libxcomposite1 libxcursor1 libxdamage1 \
    libxfixes3 libxslt1.1 libxxf86vm1

# fonts!
RUN apt-get install -y xfonts-100dpi xfonts-75dpi xfonts-base \
    xfonts-intl-asian xfonts-intl-chinese xfonts-intl-chinese-big \
    xfonts-intl-japanese xfonts-intl-japanese-big xkb-data ksh libxft2:i386


USER libby

ADD ./scripts/* ${SCRIPTS_DIR}/

RUN bash -c "echo \". ${SCRIPTS_DIR}/env\"" >> $HOME/.bashrc
ENTRYPOINT ["/bin/bash", "-c"]
CMD ["${SCRIPTS_DIR}/entrypoint"]
